from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql import functions
import os

os.environ['PYSPARK_PYTHON'] = '/home/maria_dev/miniconda3/envs/spark_env/bin/python'


def load_movie_names():
    movies_names = {}
    with open("data/u.item", encoding="ISO-8859-1") as f:
        for line in f:
            line_data = line.split("|")
            movies_names[int(line_data[0])] = line_data[1]

    return movies_names


def parse_raw_rating_data(line):
    fields = line.split()
    return Row(movieID=int(fields[1]), rating=float(fields[2]))


if __name__ == '__main__':
    spark = SparkSession.builder.appName("Worst movies ver 2").getOrCreate()

    # Map movie_id with movie_name movies_name = {movie_id: movie_name}
    movie_names = load_movie_names()

    # Raw rating data from hadoop
    raw_rating_data = spark.sparkContext.textFile("hdfs://sandbox-hdp.hortonworks.com:8020/user/maria_dev/ml-100k/u.data")

    # Convert raw rating data to object Row
    raw_movie_ratings_row = raw_rating_data.map(parse_raw_rating_data)

    # Create dataframe from Rows
    movie_rating_df = spark.createDataFrame(raw_movie_ratings_row)

    # Calculate avg ratings
    avg_rating = movie_rating_df.groupBy("movieID").avg("rating")

    # Count movie exist times
    movie_times_count = movie_rating_df.groupBy("movieID").count()

    # Join avg_rating and movie_times_count
    avg_rating_and_count = avg_rating.join(movie_times_count, "movieID")

    # Get only movie have 10 rating
    legit_rated_movie = avg_rating_and_count.filter("count >= 10")

    top_ten = legit_rated_movie.orderBy("avg(rating)").take(10)

    for movie in top_ten:
        print("#####################")
        print("Movie name: " + movie_names[movie[0]])
        print("Movie id: " + str(movie[0]))
        print("Avg rating: " + str(movie[1]))
        print("#####################")

    # Stop the session
    spark.stop()
