# spark_with_python
### Preparation for windows
* Add __sandbox-hdp.hortonworks.com__ to `C:\Windows\System32\drivers\etc\hosts`

### References
[Hortonworks tutorial](http://indico.ictp.it/event/8170/session/10/contribution/21/material/0/1.pdf)

[Basic Rdd](https://nbviewer.jupyter.org/github/ongxuanhong/data-science-works/blob/master/pyspark/study_apache_spark/rdd_co_ban.ipynb)

[Basic Dataframe](https://nbviewer.jupyter.org/github/ongxuanhong/data-science-works/blob/master/pyspark/study_apache_spark/dataframe_co_ban.ipynb)